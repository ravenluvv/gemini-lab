# Gemini-Lab

A simple Flask app that uses Gemini API to get image description and text response.

## Features

- Get text response 
- Get image description from direct upload or image url

## Prerequisites

- [Docker](https://www.docker.com/)
- [Google Gemini API](https://ai.google.dev/docs/gemini_api_overview)

### Running the app

```bash
docker build -t gemini_app .

docker run -it -p 5000:5000 gemini_app
```

### Testing the app

- only text input:

```bash
curl --location --request POST 'http://127.0.0.1:5000/text' \
--header 'Content-Type: application/json' \
--data-raw '{"text":"sing a song"}'
```

- direct image upload

```bash
curl --location --request POST 'http://127.0.0.1:5000/vision' \
--form 'image=@"/Users/tungdo/Downloads/175930811_1932004290282386_2516421897247375909_n.jpg"' \
--form 'prompt="talk about this guy"'
```

- image url

```bash
curl --location --request POST 'http://127.0.0.1:5000/vision' \
--form 'image_url="https://image-cdn.hypb.st/https%3A%2F%2Fhypebeast.com%2Fimage%2F2022%2F11%2Fchainsaw-man-pop-parade-chainsaw-man-figure-release-info-004.jpg?cbr=1&q=90"' \
--form 'prompt="talk about this guy"'
```
