import os

from dotenv import load_dotenv
from flask import Flask, jsonify, request
import google.generativeai as genai
import utils

app = Flask(__name__)
load_dotenv()
genai.configure(api_key=os.getenv('GOOGLE_API_KEY'))

text_model = genai.GenerativeModel('gemini-pro')

vision_model = genai.GenerativeModel('gemini-pro-vision')


@app.route('/')
def index():
    return 'Hello World'


@app.route('/text', methods=['POST'])
def generate_description_from_text():
    body_request = request.get_json()
    text = body_request['text']
    response = text_model.generate_content(text)
    return jsonify({"response": response.text}), 200


@app.route('/vision', methods=['POST'])
def generate_description_from_image():
    # Allow direct upload of image and image url
    image_url = request.form.get('image_url')
    image_file = request.files.get('image')

    if image_url:
        image = utils.download_image(image_url)
    elif image_file:
        image = utils.load_image(image_file)
    else:
        return jsonify({"response": "No image provided"}), 400

    prompt = request.form.get('prompt', '')

    response = vision_model.generate_content([prompt, image])

    return jsonify({"response": response.text}), 200


if __name__ == '__main__':
    app.run(debug=True)
