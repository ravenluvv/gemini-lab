from io import BytesIO
from PIL import Image
import numpy as np

import requests


def load_image(filename):
    img = Image.open(filename)
    return img


def download_image(url):
    response = requests.get(url)
    img = Image.open(BytesIO(response.content))
    return img
